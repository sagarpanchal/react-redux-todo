export const changeInput = data => ({ type: "TODO_UPDATE_INPUT", data });

export const updateList = data => ({ type: "TODO_UPDATE_LIST", data });

export const updateFilter = data => ({ type: "TODO_UPDATE_FILTER", data });
