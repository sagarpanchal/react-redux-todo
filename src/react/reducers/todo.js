const initState = {
  input: {
    type: "default",
    text: ""
  },
  list: [],
  show: "all"
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case "TODO_UPDATE_INPUT":
      return { ...state, input: { ...action.data } };

    case "TODO_UPDATE_LIST":
      // localStorage.setItem(
      //   "todo-state",
      //   JSON.stringify({ ...state, list: [...action.data] })
      // );
      return { ...state, list: [...action.data] };

    case "TODO_UPDATE_FILTER": {
      return { ...state, show: action.data };
    }

    default:
      return state;
  }
};

export default reducer;
