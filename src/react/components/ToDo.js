import React, { Component } from "react";
import PropTypes from "prop-types";

import Notifications from "react-notify-toast";
import { notify } from "react-notify-toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, ButtonGroup } from "reactstrap";
import { Input, InputGroup, InputGroupAddon } from "reactstrap";
import { ListGroup, ListGroupItem } from "reactstrap";

import { connect } from "react-redux";
import { changeInput, updateList, updateFilter } from "../actions";

import data from "./ToDo.json";

class ToDo extends Component {
  constructor() {
    super();
    this.notify = React.createRef();
  }

  componentDidMount() {
    // if (localStorage.getItem("todo-state") !== null) {
    //   let store = { ...JSON.parse(localStorage.getItem("todo-state")) };
    //   this.props.updateList(store.list);
    // }
    // window.onfocus = function() {
    //   let store = { ...JSON.parse(localStorage.getItem("todo-state")) };
    //   if (store.list !== undefined) {
    //     this.props.updateList(store.list);
    //   }
    // }.bind(this);
  }

  clearField = () => {
    let { input } = this.props;
    input.text = "";
    this.props.changeInput(input);
  };

  inputChanged = e => {
    let { input } = this.props;
    input.text = e.target.value;
    this.props.changeInput(input);
  };

  typeChanged = e => {
    let { input } = this.props;
    input.type = e.target.value.toLowerCase();
    this.props.changeInput(input);
  };

  inputKeyPressed = e => {
    if (e.key === "Enter") {
      this.addTodo();
    }
  };

  checkDuplicate = (list, text) => {
    let duplicate = false;
    list.map(
      listItem => (listItem.text === text.trim() ? (duplicate = true) : null)
    );
    return duplicate;
  };

  addTodo = () => {
    const { text, type } = this.props.input;
    const { list } = this.props;

    if (text.trim() === "") {
      notify.show("Please enter text", "info", 2000);
      return false;
    }

    if (this.checkDuplicate(list, text)) {
      notify.show("Duplicate entries aren't allowed", "info", 2000);
      return false;
    }

    this.clearField();
    list.push({ text: text.trim(), type: type.toLowerCase(), done: false });

    this.props.updateList(list);
    notify.show("Added: " + text, "success", 1500);
  };

  deleteListItem = index => {
    let { list } = this.props;
    list.splice(index, 1);
    this.props.updateList(list);
    notify.show("Deleted", "info", 2000);
  };

  toggleListItem = index => {
    let { list } = this.props;
    list[index].done ? (list[index].done = false) : (list[index].done = true);
    this.props.updateList(list);
  };

  getClassList = index => {
    const { list, show } = this.props;
    return (
      "todo-entry border-radius-0" +
      ((!list[index].done && show === "completed") ||
      (list[index].done && show === "due")
        ? " d-none"
        : "")
    );
  };

  render() {
    const { states, toastColors, types } = data;
    const { list, input, show } = this.props;
    return (
      <div className="container-fluid p-3">
        <Notifications options={{ colors: toastColors }} />
        <h2 className="text-center">Todo</h2>
        <div className="d-flex flex-row justify-content-center my-3">
          <InputGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
            {/* Input */}
            <Input
              type="text"
              placeholder="Add Task"
              value={input.text}
              onChange={this.inputChanged}
              onKeyPress={this.inputKeyPressed}
              className="border-radius-0"
            />
            <InputGroupAddon addonType="append">
              {/* select */}
              <select className="custom-select" onChange={this.typeChanged}>
                {types.map((option, index) => (
                  <option key={index}>{option}</option>
                ))}
              </select>
            </InputGroupAddon>
            <InputGroupAddon addonType="append">
              {/* Button */}
              <Button
                color="secondary"
                onClick={this.addItem}
                className="border-radius-0"
              >
                <FontAwesomeIcon icon="plus" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </div>
        {/* Button Group */}
        <div className="d-flex flex-row justify-content-center mb-3">
          <ButtonGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
            {states.map((option, index) => (
              <Button
                onClick={() => this.props.updateFilter(option.toLowerCase())}
                key={index}
                outline={show !== option.toLowerCase() ? true : null}
                className="col-lg-4"
              >
                {option}
              </Button>
            ))}
          </ButtonGroup>
        </div>
        {/* Todo List */}
        <div className="d-flex flex-row justify-content-center">
          <ListGroup className="col-md-8 col-lg-5 col-xl-4 p-0" id="todo-list">
            {!list.length ? "No items in the list." : null}
            {list.map((listItem, index) => (
              <ListGroupItem
                color={listItem.type}
                key={index}
                className={this.getClassList(index)}
              >
                <span
                  className={
                    "icon-check " + (!listItem.done ? "icon-muted" : null)
                  }
                >
                  <FontAwesomeIcon icon="check-square" />
                </span>
                <div
                  className="check-item"
                  onClick={() => this.toggleListItem(index)}
                />
                <div
                  className={
                    "list-item-text" +
                    (listItem.done ? " list-item-done text-muted" : "")
                  }
                >
                  {listItem.text}
                </div>
                <span className="icon-delete">
                  <FontAwesomeIcon icon="trash" />
                </span>
                <div
                  className="delete-item"
                  onClick={() => this.deleteListItem(index)}
                />
              </ListGroupItem>
            ))}
          </ListGroup>
        </div>
        {/* ListItem Preview */}
        {input.text !== "" ? (
          <div className="d-flex flex-row justify-content-center mt-1">
            <ListGroup className="col-md-8 col-lg-5 col-xl-4 p-0">
              <ListGroupItem
                color={input.type}
                className="todo-entry border-secondary border-radius-0 animated slideInUp faster"
              >
                {input.text}
              </ListGroupItem>
            </ListGroup>
          </div>
        ) : null}
      </div>
    );
  }
}

ToDo.propTypes = {
  input: PropTypes.object,
  list: PropTypes.array,
  show: PropTypes.string,
  changeInput: PropTypes.func,
  updateList: PropTypes.func,
  updateFilter: PropTypes.func
};

export default connect(
  state => {
    const { input, list, show } = state.todo;
    const { text, type } = input;
    return {
      input: { text, type },
      list,
      show
    };
  },
  dispatch => {
    return {
      changeInput: data => {
        dispatch(changeInput(data));
      },
      updateList: list => {
        dispatch(updateList(list));
      },
      updateFilter: filter => {
        dispatch(updateFilter(filter));
      }
    };
  }
)(ToDo);
