import React, { Component } from "react";
import "./libraries/FontAwesome";
import ToDo from "./components/ToDo";

class App extends Component {
  render() {
    return <ToDo />;
  }
}

export default App;
