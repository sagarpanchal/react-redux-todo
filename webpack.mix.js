const mx = require("laravel-mix");
const fs = require("fs-extra");

if (mx.inProduction()) {
  fs.unlink("public/assets/js/app.js.map");
  fs.unlink("public/assets/css/app.css.map");
} else {
  mx.webpackConfig({ devtool: "source-map" }).sourceMaps();
}

mx
  .options({ processCssUrls: false })
  .setPublicPath("public/assets")
  .react("src/react/index.js", "public/assets/js/app.js")
  .sass("src/sass/app.scss", "public/assets/css/app.css");
